export default {
    image: function() {
        var base_url = window.location.origin;

        return base_url + '/assets/icon/loading.gif';
    }
};